﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace TeamExplorer
{
    public class XmlSerialize<T>
    {
        private readonly string _fileName;
        public XmlSerialize(string fileName)
        {
            _fileName = fileName;
        }

        public void Serialize(List<T> seriazableCollection)
        {
            if (seriazableCollection != null)
            {
                using (var fs = new FileStream(_fileName, FileMode.OpenOrCreate))
                {
                    var xs = new XmlSerializer(typeof(List<T>));
                    xs.Serialize(fs, seriazableCollection);
                }
            }
        }
        public List<T> Deserialize()
        {
           using (var fs = new FileStream(_fileName,FileMode.OpenOrCreate))
            {
                if (fs.Length > 0)
                {
                    var xs = new XmlSerializer(typeof(List<T>));
                    return (List<T>)xs.Deserialize(fs);
                }
            }
            return new List<T>();
        }
        public void ClearXmlFile()
        {
            using (var fs = new FileStream(_fileName, FileMode.Open))
            {
                fs.SetLength(0);
            }
        }

    }
}
