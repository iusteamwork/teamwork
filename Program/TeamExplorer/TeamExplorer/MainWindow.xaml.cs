﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TeamExplorer.View;
using TeamExplorer.View.Task;
using TeamExplorer.View.Team;
using TeamExplorer.View.User;

namespace TeamExplorer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public UserDeveloper userDevoloper { get; set; }
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void UserDevoloper_Loaded(object sender, RoutedEventArgs e)
        {
            AuthorizationViev autViev = new AuthorizationViev();
            autViev.Owner = this;
            autViev.ShowDialog();
            UserTasks.AutoGenerateColumns = true;
            UserTasks.ItemsSource = userDevoloper.FindTasks();
        }

        private void FindButton_Click(object sender, RoutedEventArgs e)
        {
            TaskData tasDat = new TaskData();
            tasDat.TaskName = TaskNameTextBox.Text;
            tasDat.Status = StatusTextBox.Text;
            List<TaskData> TasDtMd = userDevoloper.FindTasks(tasDat);
            UserTasks.ItemsSource = TasDtMd; 
        }

        private void StartTimerButtonClick(object sender, RoutedEventArgs e)
        {
           // userDevoloper.StartTask(UserTasks.);
        }

        private void StopTimerClick(object sender, RoutedEventArgs e)
        {
            //userDevoloper.StopTask();
        }

        private void ProjectControl(object sender, RoutedEventArgs e)
        {
            new ProjectControl().ShowDialog();
        }

        private void UserControl(object sender, RoutedEventArgs e)
        {
            new TeamExplorer.View.User.UserControl().ShowDialog();
        }

        private void TeamControl(object sender, RoutedEventArgs e)
        {
            new TeamControl().ShowDialog();
        }

        private void TaskControl(object sender, RoutedEventArgs e)
        {
            new ViewTeamLead().ShowDialog();
        }

    }
}
