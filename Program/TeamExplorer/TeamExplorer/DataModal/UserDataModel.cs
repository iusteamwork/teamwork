﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using TeamExplorer.Repository;

namespace TeamExplorer.DataModal
{
    public class UserDataModel : ITableDataModel<UserData>
    {
        private List<UserData> _userList;
        private IStorage<UserData> IData;

        public UserDataModel()
        {
            IData = new UserRepository();
            _userList = new List<UserData>();
            Get();
        }
        /// <summary>
        /// Метод для проверки
        /// на корректность значений,
        /// введенных пользователем.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool Verify(UserData obj)
        {
            if (!String.IsNullOrWhiteSpace(obj.FIO) &&
                Regex.IsMatch(obj.FIO, @"[а-яіїґєэъыА-ЯІЇҐЄЭa-zA-Z]") &&
                !String.IsNullOrWhiteSpace(obj.IdUserType) &&
                !String.IsNullOrWhiteSpace(obj.Login) &&
                !String.IsNullOrWhiteSpace(obj.IdTeam) &&
                Regex.IsMatch(obj.Login, @"[а-яіїґєэъыА-ЯІЇҐЄЭa-zA-Z]") &&
                !String.IsNullOrWhiteSpace(obj.IdUser) &&
                !String.IsNullOrWhiteSpace(obj.Password) &&
                Regex.IsMatch(obj.Password, @"[а-яіїґєэъыА-ЯІЇҐЭЪЫa-zA-Z0-9]{4,10}"))
                return true;
            else
                return false;
        }
        /// <summary>
        /// Метод для добавления
        /// нового объекта в коллекцию.
        /// </summary>
        /// <param name="obj"></param>
        public bool AddNew(UserData obj)
        {
            Guid userGuid = new Guid(); // Генерация ключа для каждого нового объекта.
            userGuid = Guid.NewGuid();
            obj.IdUser = userGuid.ToString();
            if (Verify(obj))
            { 
                _userList.Add(obj);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Метод для удаления
        /// объектов из коллекции.
        /// </summary>
        /// <param name="id"></param>
        public bool Remove(string id)
        {
            if (!String.IsNullOrWhiteSpace(id))
               return _userList.Remove(_userList.First(u => u.IdUser == id));
            return false;
        }
        /// <summary>
        /// Метод для поиска
        /// по коллекции.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public List<UserData> Find(UserData obj)
        {
            List<UserData> findList = new List<UserData>(_userList);

            if (!String.IsNullOrWhiteSpace(obj.FIO) &&
                !String.IsNullOrWhiteSpace(obj.IdUserType) &&
                !String.IsNullOrWhiteSpace(obj.Login) &&
                !String.IsNullOrWhiteSpace(obj.IdTeam) &&
                !String.IsNullOrWhiteSpace(obj.IdUser) &&
                !String.IsNullOrWhiteSpace(obj.Password))
                return null;
            else
            {
                if (!String.IsNullOrWhiteSpace(obj.FIO))
                {
                    findList = findList.Where(c => c.FIO == obj.FIO).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.IdUserType))
                {
                    findList = findList.Where(c => c.IdUserType == obj.IdUserType).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.Login))
                {
                    findList = findList.Where(c => c.Login == obj.Login).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.IdTeam))
                {
                    findList = findList.Where(c => c.IdTeam == obj.IdTeam).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.IdUser))
                {
                    findList = findList.Where(c => c.IdUser == obj.IdUser).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.Password))
                {
                    findList = findList.Where(c => c.Password == obj.Password).Select(c => c).ToList();
                }
                if (findList.Count == 0)
                {
                    return null;
                }
                return findList;
            }
        }
        /// <summary>
        /// Метод для изменения
        /// значений объекта.
        /// </summary>
        /// <param name="obj"></param>
        public void Update(UserData obj)
        {
            if (Verify(obj))
            {
                var id = _userList.Where(c => c.IdUser == obj.IdUser).Select(c => c).FirstOrDefault();
                id = obj;
            }    
        }
        
        public void Get()
        {
            _userList = IData.ReadTable();
        }
        
        public void Set()
        {
            IData.UpdateTable(_userList);
        }
        
        public List<UserData> SetList()
        {
            return _userList;
        }
    }
}
