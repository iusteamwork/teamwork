﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TeamExplorer.Repository;

namespace TeamExplorer
{
    class UserTypeDataModel : ITableDataModel<UserTypeData>
    {
        private List<UserTypeData> _userTypeList;

        private IStorage<UserTypeData> IData;
        
        public UserTypeDataModel()
        {
            IData = new UserTypeRepository();
            _userTypeList = new List<UserTypeData>();
        }
        /// <summary>
        /// Метод для проверки
        /// на корректность значений,
        /// введенных пользователем.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool Verify(UserTypeData obj)
        {
            if (!String.IsNullOrWhiteSpace(obj.IdUserType) &&
                !String.IsNullOrWhiteSpace(obj.AccessLevel) &&
                Regex.IsMatch(obj.AccessLevel, @"[а-яіїґєэъыА-ЯІЇҐЄЭa-zA-Z]") &&
                !String.IsNullOrWhiteSpace(obj.NameType) &&
                Regex.IsMatch(obj.NameType, @"[а-яіїґєэъыА-ЯІЇҐЄЭa-zA-Z]"))
                return true;
            else
                return false;
        }
        /// <summary>
        /// Метод для добавления
        /// нового объекта в коллекцию.
        /// </summary>
        /// <param name="obj"></param>
        public bool AddNew(UserTypeData obj)
        {
            Guid userGuid = new Guid(); // Генерация ключа для каждого нового объекта.
            userGuid = Guid.NewGuid();
            obj.IdUserType = userGuid.ToString();
            if (Verify(obj))
            {
                _userTypeList.Add(obj);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Метод для удаления
        /// объектов из коллекции.
        /// </summary>
        /// <param name="id"></param>
        public bool Remove(string id)
        {
            if (!String.IsNullOrWhiteSpace(id))
               return _userTypeList.Remove(_userTypeList.First(u => u.IdUserType == id));
            return false;
        }
        /// <summary>
        /// Метод для поиска
        /// по коллекции.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public List<UserTypeData> Find(UserTypeData obj)
        {
            List<UserTypeData> findList = new List<UserTypeData>(_userTypeList.Count);

            if (!String.IsNullOrWhiteSpace(obj.AccessLevel) &&
                !String.IsNullOrWhiteSpace(obj.IdUserType) &&
                !String.IsNullOrWhiteSpace(obj.NameType))
                return null;
            else
            {
                if (!String.IsNullOrWhiteSpace(obj.AccessLevel))
                {
                    findList = findList.Where(c => c.AccessLevel == obj.AccessLevel).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.IdUserType))
                {
                    findList = findList.Where(c => c.IdUserType == obj.IdUserType).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.NameType))
                {
                    findList = findList.Where(c => c.NameType == obj.NameType).Select(c => c).ToList();
                }
                if (findList.Count == 0)
                {
                    return null;
                }
                return findList;
            }

        }
        /// <summary>
        /// Метод для изменения
        /// значений объекта.
        /// </summary>
        /// <param name="obj"></param>
        public void Update(UserTypeData obj)
        {
            if (Verify(obj))
            {
                var id = _userTypeList.Where(c => c.IdUserType == obj.IdUserType).Select(c => c).FirstOrDefault();
                id = obj;
            }
        }
        
        public void Get()
        {
            _userTypeList = IData.ReadTable();
        }

        public void Set()
        {
            IData.UpdateTable(_userTypeList);
        }

        public List<UserTypeData> SetList()
        {
            return _userTypeList;
        }
    }
}
