﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TeamExplorer
{
    public class TeamDataModel : ITableDataModel<TeamData>
    {
        private List<TeamData> _teamList;
        private IStorage<TeamData> IData;
        
        public TeamDataModel()
        {
            IData = new TeamRepository();
            _teamList = new List<TeamData>();
        }
        /// <summary>
        /// Метод для проверки
        /// на корректность значений,
        /// введенных пользователем.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool Verify(TeamData obj)
        {
            if (!String.IsNullOrWhiteSpace(obj.IdTeam) &&
                !String.IsNullOrWhiteSpace(obj.NameTeam) &&
                Regex.IsMatch(obj.NameTeam, @"[а-яіїґєэъыА-ЯІЇҐЄЭa-zA-Z]"))
                return true;
            else
                return false;
        }
        /// <summary>
        /// Метод для добавления
        /// нового объекта в коллекцию.
        /// </summary>
        /// <param name="obj"></param>
        public bool AddNew(TeamData obj)
        {
            Guid userGuid = new Guid(); // Генерация ключа для каждого нового объекта.
            userGuid = Guid.NewGuid();
            obj.IdTeam = userGuid.ToString();
            if (Verify(obj))
            {    
                _teamList.Add(obj);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Метод для удаления
        /// объектов из коллекции.
        /// </summary>
        /// <param name="id"></param>
        public bool Remove(string id)
        {
            if (!String.IsNullOrEmpty(id))
               return _teamList.Remove(_teamList.First(u => u.IdTeam == id));
            return false;
        }
        /// <summary>
        /// Метод для поиска
        /// по коллекции.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public List<TeamData> Find(TeamData obj)
        {
            List<TeamData> findList = new List<TeamData>(_teamList.Count);

            if (!String.IsNullOrWhiteSpace(obj.IdTeam) &&
                !String.IsNullOrWhiteSpace(obj.NameTeam))
                return null;
            else
            {
                if (!String.IsNullOrWhiteSpace(obj.IdTeam))
                {
                    findList = findList.Where(c => c.IdTeam == obj.IdTeam).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.NameTeam))
                {
                    findList = findList.Where(c => c.NameTeam == obj.NameTeam).Select(c => c).ToList();
                }
                if (findList.Count == 0)
                {
                    return null;
                }
                return findList;
            }
        }
        /// <summary>
        /// Метод для изменения
        /// значений объекта.
        /// </summary>
        /// <param name="obj"></param>
        public void Update(TeamData obj)
        {
            if (Verify(obj))
            {
                var id = _teamList.Where(c => c.IdTeam == obj.IdTeam).Select(c => c).FirstOrDefault();
                id = obj;
            } 
        }
        
        public void Get()
        {
            _teamList = IData.ReadTable();
        }

        public void Set()
        {
            IData.UpdateTable(_teamList);
        }

        public List<TeamData> SetList()
        {
            return _teamList;
        }
    }
}
