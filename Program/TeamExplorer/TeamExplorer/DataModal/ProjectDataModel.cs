﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TeamExplorer.Repository;

namespace TeamExplorer
{
    public class ProjectDataModel : ITableDataModel<ProjectData>
    {
        private List<ProjectData> _projectDataList;

        private IStorage<ProjectData> IData;

        public ProjectDataModel()
        {
            IData = new ProjectRepository();
            _projectDataList = new List<ProjectData>();
        }

        private bool Verify(ProjectData obj) // Метод для проверки значений, введенных пользователем, на корректность.
        {
            if (
                obj.DateBegin != DateTime.MinValue
                && obj.DateEnd != DateTime.MinValue
                && !String.IsNullOrWhiteSpace(obj.ProjectName)
                && !String.IsNullOrWhiteSpace(obj.Status)
                && Regex.IsMatch(obj.Status, @"[а-яА-Яa-zA-ZіїґєэъыІЇҐЄЭ]")
                && Regex.IsMatch(obj.ProjectName, @"[а-яА-Яa-zA-ZіїґєэъыІЇҐЄЭ]")
                )
            {
                var projects=_projectDataList.Where(p=>p.ProjectName.Equals(obj.ProjectName));
                if(!projects.Any()) return true;     
            }
            return false;
        }

        public bool AddNew(ProjectData obj)   // Метод для добавления нового объекта в коллекцию.
        {
            Guid userGuid = new Guid();
            userGuid = Guid.NewGuid();        // Генерация ключа для каждого нового объекта.
            obj.IdProject = userGuid.ToString();
            if (Verify(obj))
            {
                _projectDataList.Add(obj);
                return true;
            }
            return false;
        }

        public bool Remove(string id)   // Метод для удаления объектов из коллекции. 
        {
            if (!String.IsNullOrWhiteSpace(id))
            {
                Get();
                return _projectDataList.Remove(_projectDataList.First(u => u.IdProject == id));
            }
            return false;
        }

        public List<ProjectData> Find(ProjectData obj)  // Метод для поиска в коллекции.
        {
            List<ProjectData> findList = new List<ProjectData>(_projectDataList.Count);
            if (!String.IsNullOrWhiteSpace(obj.IdProject) && !String.IsNullOrWhiteSpace(obj.ProjectName) && !String.IsNullOrWhiteSpace(obj.Status)
                && (obj.DateBegin != DateTime.MinValue) && (obj.DateEnd != DateTime.MinValue)) return null;
            else
            {
                if (!String.IsNullOrWhiteSpace(obj.IdProject))
                {
                    findList = findList.Where(c => c.IdProject == obj.IdProject).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.ProjectName))
                {
                    findList = findList.Where(c => c.ProjectName == obj.ProjectName).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.Status))
                {
                    findList = findList.Where(c => c.Status == obj.Status).Select(c => c).ToList();
                }
                if (obj.DateBegin != DateTime.MinValue)
                {
                    findList = findList.Where(c => c.DateBegin == obj.DateBegin).Select(c => c).ToList();
                }
                if (obj.DateEnd != DateTime.MinValue)
                {
                    findList = findList.Where(c => c.DateEnd == obj.DateEnd).Select(c => c).ToList();
                }
                return findList;
            }
        }

        public void Update(ProjectData obj)  // Метод для изменения значений объекта.s
        {
            if (Verify(obj))
            {
                var elem = _projectDataList.Where(c => c.IdProject == obj.IdProject).Select(c => c).FirstOrDefault();
                elem = obj;
            }
        }

        public void Get()
        {
            _projectDataList = IData.ReadTable();
        }

        public void Set()
        {
            IData.UpdateTable(_projectDataList);
        }

        public List<ProjectData> SetList()
        {
            return _projectDataList;
        }
    }
}
