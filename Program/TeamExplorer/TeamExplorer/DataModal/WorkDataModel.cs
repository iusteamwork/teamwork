﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TeamExplorer
{
    class WorkDataModel : ITableDataModel<WorkData>
    {
        private List<WorkData> _workDataList;

        private IStorage<WorkData> IData;

        public WorkDataModel()
        {
            IData = new WorkRepository();
            _workDataList = new List<WorkData>();
        }

        private bool Verify(WorkData obj) // Метод для проверки значений, введенных пользователем, на корректность.
        {
            if (
                !String.IsNullOrWhiteSpace(obj.IdTask)
                && !String.IsNullOrWhiteSpace(obj.IdUser)
                && obj.TimeWork != DateTime.MinValue
                )
                return true;
            else return false;
        }

        public bool AddNew(WorkData obj) // Метод для добавления нового объекта в коллекцию.
        {
            Guid userGuid = new Guid(); // Генерация ключа для каждого нового объекта.
            userGuid = Guid.NewGuid();
            obj.IdWorks = userGuid.ToString();
            if (Verify(obj))
            {
                _workDataList.Add(obj);
                return true;
            }
            return false;
        }

        public bool Remove(string id) // Метод для удаления объектов из коллекции. 
        {
            if (!String.IsNullOrWhiteSpace(id))
               return _workDataList.Remove(_workDataList.First(u => u.IdWorks == id));
            return false;
        }

        public List<WorkData> Find(WorkData obj) // Метод для поиска в коллекции.
        {

            List<WorkData> findList = new List<WorkData>(_workDataList);

            if (!String.IsNullOrWhiteSpace(obj.IdWorks) && !String.IsNullOrWhiteSpace(obj.IdTask) && !String.IsNullOrWhiteSpace(obj.IdUser)
               && obj.TimeWork != DateTime.MinValue)
                return null;
            else
            {
                if (!String.IsNullOrWhiteSpace(obj.IdWorks))
                {
                    findList = findList.Where(c => c.IdWorks == obj.IdWorks).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.IdUser))
                {
                    findList = findList.Where(c => c.IdUser == obj.IdUser).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.IdTask))
                {
                    findList = findList.Where(c => c.IdTask == obj.IdTask).Select(c => c).ToList();
                }
                if (obj.TimeWork != DateTime.MinValue)
                {
                    findList = findList.Where(c => c.TimeWork == obj.TimeWork).Select(c => c).ToList();
                }
                return findList;
            }
        }

        public void Update(WorkData obj) // Метод для изменения значений объекта.
        {
            if (Verify(obj))
            {
                var elem = _workDataList.Where(c => c.IdWorks == obj.IdWorks).Select(c => c).FirstOrDefault();
                elem = obj;
            }
        }

        public void Get()
        {
            _workDataList = IData.ReadTable();
        }

        public void Set()
        {
            IData.UpdateTable(_workDataList);
        }

        public List<WorkData> SetList()
        {
            return _workDataList;
        }


    }


}
