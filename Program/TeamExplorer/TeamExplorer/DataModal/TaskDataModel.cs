﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TeamExplorer.Repository;

namespace TeamExplorer
{
    class TaskDataModel : ITableDataModel<TaskData>
    {
        private List<TaskData> _taskDataList;

        private IStorage<TaskData> IData;

        public TaskDataModel()
        {
            IData = new TaskRepository();
            _taskDataList = new List<TaskData>();
        }

        private bool Verify(TaskData obj)   // Метод для проверки значений, введенных пользователем, на корректность.
        {
            if (
                !String.IsNullOrWhiteSpace(obj.IdProject)
                && !String.IsNullOrWhiteSpace(obj.Status)
                && !String.IsNullOrWhiteSpace(obj.TaskName)
                && (obj.Time != DateTime.MinValue)
                && Regex.IsMatch(obj.Status, @"[а-яА-Яa-zA-ZіїґєэъыІЇҐЄЭ]")
                && Regex.IsMatch(obj.TaskName, @"[а-яА-Яa-zA-ZіїґєэъыІЇҐЄЭ]")
                )
                return true;
            else return false;
        }

        public bool AddNew(TaskData obj) // Метод для добавления нового объекта в коллекцию.
        {
            Guid userGuid = new Guid(); // Генерация ключа для каждого нового объекта.
            userGuid = Guid.NewGuid();
            obj.IdTask = userGuid.ToString();
            if (Verify(obj))
            {
                _taskDataList.Add(obj);
                return true;
            }
            return false;
        }

        public bool Remove(string id) // Метод для удаления объектов из коллекции. 
        {
            if (!String.IsNullOrWhiteSpace(id))
               return _taskDataList.Remove(_taskDataList.First(u => u.IdTask == id));
            return false;
        }

        public List<TaskData> Find(TaskData obj) // Метод для поиска в коллекции.
        {
            List<TaskData> findList = new List<TaskData>(_taskDataList);

            if (!String.IsNullOrWhiteSpace(obj.IdTask) && !String.IsNullOrWhiteSpace(obj.IdProject) && !String.IsNullOrWhiteSpace(obj.Status)
               && !String.IsNullOrWhiteSpace(obj.TaskName) && (obj.Time != DateTime.MinValue))
                return null;
            else
            {
                if (!String.IsNullOrWhiteSpace(obj.IdTask))
                {
                    findList = findList.Where(c => c.IdTask == obj.IdTask).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.IdProject))
                {
                    findList = findList.Where(c => c.IdProject == obj.IdProject).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.Status))
                {
                    findList = findList.Where(c => c.Status == obj.Status).Select(c => c).ToList();
                }
                if (!String.IsNullOrWhiteSpace(obj.TaskName))
                {
                    findList = findList.Where(c => c.TaskName == obj.TaskName).Select(c => c).ToList();
                }
                if (obj.Time != DateTime.MinValue)
                {
                    findList = findList.Where(c => c.Time == obj.Time).Select(c => c).ToList();
                }
                return findList;
            }
        }

        public void Update(TaskData obj) // Метод для изменения значений объекта.
        {
            if (Verify(obj))
            {
                var elem = _taskDataList.Where(c => c.IdTask == obj.IdTask).Select(c => c).FirstOrDefault();
                elem = obj;
            }
        }

        public void Get()
        {
            _taskDataList = IData.ReadTable();
        }

        public void Set()
        {
            IData.UpdateTable(_taskDataList);
        }

        public List<TaskData> SetList()
        {
            return _taskDataList;
        }
    }

}
