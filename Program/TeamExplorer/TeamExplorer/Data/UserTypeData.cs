﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamExplorer
{
    class UserTypeData
    {
        public string AccessLevel { get; set; }
        public string IdUserType { get; set; }
        public string NameType { get; set; }
    }
}
