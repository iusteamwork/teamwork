﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamExplorer
{
    public class ProjectData
    {
        public DateTime DateBegin
        {
            get;
            set;
        }

        public DateTime DateEnd
        {
            get;
            set;
        }

        public string IdProject
        {
            get;
            set;
        }

        public string ProjectName
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

    }

}
