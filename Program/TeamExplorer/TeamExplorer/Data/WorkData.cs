﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamExplorer
{
    public class WorkData
    {
        public DateTime TimeWork
        {
            get;
            set;
        }

        public string IdUser
        {
            get;
            set;
        }

        public string IdWorks
        {
            get;
            set;
        }

        public string IdTask
        {
            get;
            set;
        }
    }

}
