﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamExplorer
{
    public class TaskData
    {
        public DateTime DateCreate
        {
            get;
            set;
        }

        public DateTime DateEndTask
        {
            get;
            set;
        }

        public DateTime Time
        {
            get;
            set;
        }

        public string IdProject
        {
            get;
            set;
        }

        public string IdTask
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string TaskName
        {
            get;
            set;
        }
    }

}
