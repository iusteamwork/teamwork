﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamExplorer
{
    public class TeamData
    {
        public string IdTeam { get; set; }
        public string NameTeam { get; set; }
    }
}
