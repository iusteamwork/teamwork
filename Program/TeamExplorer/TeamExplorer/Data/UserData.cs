﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamExplorer
{
    public class UserData
    {
        public string FIO { get; set; }
        public string IdUserType { get; set; }
        public string Login { get; set; }
        public string IdTeam { get; set; }
        public string IdUser { get; set; }
        public string Password { get; set; }
    }
}
