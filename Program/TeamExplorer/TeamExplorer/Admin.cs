﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamExplorer.DataModal;

namespace TeamExplorer
{
    class Admin
    {
        private UserDataModel _udm = new UserDataModel();
        private  TeamDataModel _tdm = new TeamDataModel();

        public void AddUser(UserData user)
        {
            _udm.Get();
            _udm.AddNew(user);
            _udm.Set();
        }

        public void DeleteUser(string id)
        {
            _udm.Get();
            _udm.Remove(id);
            _udm.Set();
        }

        public void UpdateUser(UserData user)
        {
            _udm.Get();
            _udm.Update(user);
            _udm.Set();
        }

        public void AddTeam(TeamData team)
        {
            _tdm.Get();
            _tdm.AddNew(team);
            _tdm.Set();
        }

        public void DeleteTeam(string idTeam)
        {
            _tdm.Get();
            _tdm.Remove(idTeam);
            _tdm.Set();
        }

        public void UpdateTeam(TeamData team)
        {
            _tdm.Get();
            _tdm.Update(team);
            _tdm.Set();
        }
    }
}
