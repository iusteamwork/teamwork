﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamExplorer
{
    public class Manager
    {
        private UserData _manager;

        public Manager()
        {
            _manager = new UserData();
        }

        public List<ProjectData> FindProject(ProjectData obj)
        {
            var workDataModel = new WorkDataModel();
            var taskDataModel = new TaskDataModel();
            var projectDataModel = new ProjectDataModel();

            workDataModel.Get();
            taskDataModel.Get();
            projectDataModel.Get();

            List<WorkData> works = workDataModel.Find(new WorkData() { IdUser = _manager.IdUser });
            var tasks = new List<TaskData>();
            var projects = new List<ProjectData>();      
            
            foreach (var work in works)
            {
                tasks.AddRange(taskDataModel.Find(new TaskData() { IdTask = work.IdTask }));
            }
            foreach (var task in tasks)
            {
                projects.AddRange(
                    projectDataModel.Find(new ProjectData()
                    {
                        DateBegin = obj.DateBegin,
                        DateEnd = obj.DateEnd,
                        IdProject = obj.IdProject,
                        ProjectName = obj.ProjectName,
                        Status = obj.Status
                    }));
            }
            return projects;
        }

        public void AddTask(ProjectData obj)
        {
            var projectDataModel = new ProjectDataModel();
            projectDataModel.Get();
            projectDataModel.AddNew(new ProjectData()
            {
                DateBegin = obj.DateBegin,
                DateEnd = obj.DateEnd,
                IdProject = obj.IdProject,
                ProjectName = obj.ProjectName,
                Status = obj.Status
            });
            var taskDataModel = new TaskDataModel();
            taskDataModel.Get();
            taskDataModel.AddNew(new TaskData()
            {
                //DateCreate = ,
                //DateEndTask = ,
                //IdProject = ,
                //Status = obj.Status,
                //TaskName = ,
                //Time = ,
                //IdTask = 
            });
            var workDataModel = new WorkDataModel();
            workDataModel.Get();
            workDataModel.AddNew(new WorkData()
            {
                //IdTask = ,
                IdUser = _manager.IdUser,
                //IdWorks=
                //TimeWork=
            });
        }

        public void DeleteProject(ProjectData obj)
        {
            var projectDataModel = new ProjectDataModel();
            projectDataModel.Get();
            projectDataModel.Remove(obj.IdProject);
        }
    }
}
