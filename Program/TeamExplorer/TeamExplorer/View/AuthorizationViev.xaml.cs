﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamExplorer
{
    /// <summary>
    /// Логика взаимодействия для AuthorizationViev.xaml
    /// </summary>
    public partial class AuthorizationViev : Window
    {
        public AuthorizationViev()
        {
            InitializeComponent();
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            List<WorkData> WorLis = new List<WorkData>();
            WorLis.Add(new WorkData() { IdTask = "0", IdUser = "fd0bf08f-1778-4f27-85c9-7b48bff95f6e", IdWorks = "0", TimeWork = DateTime.Now });
            WorkRepository wr = new WorkRepository();
            wr.CreateTable(WorLis);
            Application.Current.Shutdown();
        }

        private void AuthorizationClick(object sender, RoutedEventArgs e)
        {
            UserDeveloper user = new UserDeveloper();
            loginTextBox.Text = "azezalo";
            passwordTextBox.Password = "password";
            if (user.UserAvtorizetid(loginTextBox.Text, passwordTextBox.Password))
            {
                MainWindow mainWin = this.Owner as MainWindow;
                if (mainWin != null)
                {
                    mainWin.userDevoloper = user;
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("Совпадение логина пароля не найденно");
            }
        }
    }
}
