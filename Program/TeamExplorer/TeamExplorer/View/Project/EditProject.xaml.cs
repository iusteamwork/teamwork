﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamExplorer.View.Project
{
    /// <summary>
    /// Interaction logic for EditProject.xaml
    /// </summary>
    public partial class EditProject : Window
    {
        private ProjectData _projectItem;

        public EditProject()
        {
            InitializeComponent();
        }

        public EditProject(ProjectData _projectItem)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this._projectItem = _projectItem;
            Loaded += EditProject_Loaded;
        }

        void EditProject_Loaded(object sender, RoutedEventArgs e)
        {
            ProjectName.Text = _projectItem.ProjectName;
            ProjectStatus.Text = _projectItem.Status;
            StartDate.SelectedDate = _projectItem.DateBegin;
            EndDate.SelectedDate = _projectItem.DateEnd;
            
        }

        private void SaveProject(object sender, RoutedEventArgs e)
        {

        }

    }
}
