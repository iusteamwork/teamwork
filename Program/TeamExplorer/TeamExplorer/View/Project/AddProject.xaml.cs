﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamExplorer.View.Project
{
    /// <summary>
    /// Interaction logic for AddProject.xaml
    /// </summary>
    public partial class AddProject : Window
    {
        public AddProject()
        {
            InitializeComponent();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if(!String.IsNullOrEmpty(ProjectName.Text))
            {
                var project = new ProjectData
                {
                    IdProject=Guid.NewGuid().ToString(),
                    ProjectName = ProjectName.Text,
                    DateBegin = StartDate.SelectedDate.Value,
                    DateEnd = EndDate.SelectedDate.Value ,
                    Status = "test"
                };
                var projectDataModel = new ProjectDataModel();
                projectDataModel.Get();
                if (projectDataModel.AddNew(project))
                {
                    projectDataModel.Set();
                    MessageBox.Show("Успешно добавлен");
                }
                else MessageBox.Show("Возникла ошибка");
            }
            else MessageBox.Show("Введите имя проекта");
        }
    }
}
