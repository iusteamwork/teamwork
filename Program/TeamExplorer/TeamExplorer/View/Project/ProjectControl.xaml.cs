﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamExplorer.Repository;
using TeamExplorer.View.Project;

namespace TeamExplorer.View
{
    /// <summary>
    /// Interaction logic for ProjectControl.xaml
    /// </summary>
    public partial class ProjectControl : Window
    {
        private ProjectData _projectItem;
        public ProjectControl()
        {
            InitializeComponent();
            Loaded += ProjectControl_Loaded;
        }

        void ProjectControl_Loaded(object sender, RoutedEventArgs e)
        {
            GetData();  
        }

        private void GetData()
        {
            // Output data in datagrid
            var projectStorage = new ProjectRepository();
            var projectList = new List<ProjectData>
            {
               new ProjectData {IdProject=Guid.NewGuid().ToString(), DateBegin=DateTime.Now, DateEnd=DateTime.Now, Status="0" }
            };
            //projectStorage.CreateTable(projectList);
            try
            {
                ProjectDataGrid.ItemsSource = projectStorage.ReadTable();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); Close(); }
        }
        private void AddProject(object sender, RoutedEventArgs e)
        {
            new AddProject().ShowDialog();
        }

        private void ProjectChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if(dataGrid!=null)
            {
                _projectItem = dataGrid.SelectedItem as ProjectData;
            }
        }

        private void DeleteProject(object sender, RoutedEventArgs e)
        {
            if (_projectItem != null)
            {
                var projectDataModel = new ProjectDataModel();
                projectDataModel.Remove(_projectItem.IdProject);
                projectDataModel.Set();
            }
            else MessageBox.Show("Сначала выберите поле");
        }

        private void RefreshData(object sender, RoutedEventArgs e)
        {
            GetData();
        }

        private void EditProject(object sender, RoutedEventArgs e)
        {
            if (_projectItem != null)
            {
                new EditProject(_projectItem).ShowDialog();
            }
            else MessageBox.Show("Сначала выберите поле");
        }

    }
}
