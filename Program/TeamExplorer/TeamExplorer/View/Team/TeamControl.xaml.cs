﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamExplorer.Repository;

namespace TeamExplorer.View.Team
{
    /// <summary>
    /// Interaction logic for TeamControl.xaml
    /// </summary>
    public partial class TeamControl : Window
    {
        private TeamData _teamItem;

        public TeamControl()
        {
            InitializeComponent();
            Loaded += TeamControl_Loaded;
        }

        void TeamControl_Loaded(object sender, RoutedEventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            // Output data in datagrid
            var teamStorage = new ProjectRepository();
            var teamList = new List<TeamData>
            {
               new TeamData {IdTeam=Guid.NewGuid().ToString(), NameTeam=""}
            };
            //projectStorage.CreateTable(projectList);
            try
            {
                TeamDataGrid.ItemsSource = teamStorage.ReadTable();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); Close(); }
        }

        private void AddTeam(object sender, RoutedEventArgs e)
        {
            new AddTeam().ShowDialog();
        }

        private void TeamChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (dataGrid != null)
            {
                _teamItem = dataGrid.SelectedItem as TeamData;
            }
        }

        private void DeleteTeam(object sender, RoutedEventArgs e)
        {
            if (_teamItem != null)
            {
                var projectDataModel = new ProjectDataModel();
                projectDataModel.Remove(_teamItem.IdTeam);
            }
            else MessageBox.Show("Сначала выберите поле");
        }

        private void EditTeam(object sender, RoutedEventArgs e)
        {
            if (_teamItem != null)
            {
                new EditTeam(_teamItem).ShowDialog();
            }
            else MessageBox.Show("Сначала выберите поле");
        }



    }
}
