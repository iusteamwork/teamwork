﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamExplorer.View.Team
{
    /// <summary>
    /// Interaction logic for AddTeam.xaml
    /// </summary>
    public partial class AddTeam : Window
    {
        public AddTeam()
        {
            InitializeComponent();
        }

        private void AddNewTeam(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(NameTeam.Text))
            {
                var team = new TeamData
                {
                    IdTeam = Guid.NewGuid().ToString(),
                    NameTeam = NameTeam.Text
                };
                var teamDataModel = new TeamDataModel();
                teamDataModel.Get();
                if (teamDataModel.AddNew(team))
                {
                    teamDataModel.Set();
                    MessageBox.Show("Успешно добавлен");
                }
                else MessageBox.Show("Возникла ошибка");
            }
            else MessageBox.Show("Введите имя проекта");
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
