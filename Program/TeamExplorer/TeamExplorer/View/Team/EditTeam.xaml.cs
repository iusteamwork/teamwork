﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamExplorer.View.Team
{
    /// <summary>
    /// Interaction logic for EditTeam.xaml
    /// </summary>
    public partial class EditTeam : Window
    {
        private TeamData _teamItem;

        public EditTeam()
        {
            InitializeComponent();
        }

        public EditTeam(TeamData _teamItem)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this._teamItem = _teamItem;
            Loaded += EditTeam_Loaded;
        }
        void EditTeam_Loaded(object sender, RoutedEventArgs e)
        {
            NameTeam.Text = _teamItem.NameTeam;
        }

        private void SaveProject(object sender, RoutedEventArgs e)
        {

        }
    }
}
