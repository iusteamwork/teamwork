﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamExplorer.View.User
{
    /// <summary>
    /// Interaction logic for EditUser.xaml
    /// </summary>
    public partial class EditUser : Window
    {
        private UserData _userItem;

        public EditUser()
        {
            InitializeComponent();
        }

        public EditUser(UserData _userItem)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this._userItem = _userItem;
            Loaded += EditUser_Loaded;
        }

        void EditUser_Loaded(object sender, RoutedEventArgs e)
        {
            //ProjectName.Text = _userItem.ProjectName;
            //ProjectStatus.Text = _userItem.Status;
            //StartDate.SelectedDate = _userItem.DateBegin;
            //EndDate.SelectedDate = _userItem.DateEnd;   
        }

        private void SaveProject(object sender, RoutedEventArgs e)
        {

        }
    }
}
