﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamExplorer.DataModal;
using TeamExplorer.Repository;

namespace TeamExplorer.View.User
{
    /// <summary>
    /// Interaction logic for UserControl.xaml
    /// </summary>
    public partial class UserControl : Window
    {
        private UserData _userItem;
        public UserControl()
        {
            InitializeComponent();
            Loaded += UserControl_Loaded;
        }

        void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            var userStorage = new ProjectRepository();
            var userList = new List<UserData>
            {
               new UserData {IdUser=Guid.NewGuid().ToString(), FIO="", IdUserType="", Login="", IdTeam = "", Password = ""}
            };

            try
            {
                UserDataGrid.ItemsSource = userStorage.ReadTable();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); Close(); }
        }

        private void AddUser(object sender, RoutedEventArgs e)
        {
            new AddUser().ShowDialog();
        }

        private void UserChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (dataGrid != null)
            {
                _userItem = dataGrid.SelectedItem as UserData;
            }
        }

        private void DeleteUser(object sender, RoutedEventArgs e)
        {
            if (_userItem != null)
            {
                var userDataModel = new UserDataModel();
                userDataModel.Remove(_userItem.IdUser);
            }
            else MessageBox.Show("Сначала выберите поле");
        }

        private void EditUser(object sender, RoutedEventArgs e)
        {
            if (_userItem != null)
            {
                new EditUser(_userItem).ShowDialog();
            }
            else MessageBox.Show("Сначала выберите поле");
        }
        
    }
}
