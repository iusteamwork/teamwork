﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamExplorer.DataModal;

namespace TeamExplorer.View.User
{
    /// <summary>
    /// Interaction logic for AddUser.xaml
    /// </summary>
    public partial class AddUser : Window
    {
        public AddUser()
        {
            InitializeComponent();
        }

        private void AddNewUser(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(Surname.Text))
            {
                var project = new UserData
                {
                    IdUser = Guid.NewGuid().ToString(),
                    FIO = Surname.Text,
                    //IdUserType = ,
                    Login = Login.Text,
                    //IdTeam = ,
                    Password = Password.Text
                };
                var userDataModel = new UserDataModel();
                userDataModel.Get();
                if (userDataModel.AddNew(project))
                {
                    userDataModel.Set();
                    MessageBox.Show("Успешно добавлен");
                }
                else MessageBox.Show("Возникла ошибка");
            }
            else MessageBox.Show("Введите имя проекта");
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
