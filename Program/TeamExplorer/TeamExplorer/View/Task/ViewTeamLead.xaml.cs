﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamExplorer.Repository;

namespace TeamExplorer.View.Task
{
    /// <summary>
    /// Логика взаимодействия для ViewTeamLead.xaml
    /// </summary>
    public partial class ViewTeamLead : Window
    {
        private TaskData _taskItem;
        public ViewTeamLead()
        {
            InitializeComponent();
            Loaded += TeamLead_Loaded;
        }

        void TeamLead_Loaded(object sender, RoutedEventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            var taskStorage = new TaskRepository();
            var taskList = new List<TaskData>
            {
               new TaskData {IdTask=Guid.NewGuid().ToString(), DateCreate=DateTime.Now, DateEndTask =DateTime.Now, Time=DateTime.Now, Status ="",TaskName="" }
            };
            try
            {
                TaskDataGrid.ItemsSource = taskStorage.ReadTable();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); Close(); }
        }
        private void EditTask(object sender, RoutedEventArgs e)
        {
            if (_taskItem != null)
            {
            }
            else MessageBox.Show("Сначала выберите поле");
        }

        private void Btn_AddTask(object sender, RoutedEventArgs e)
        {
            new AddTask().ShowDialog();
        }

        private void TaskChanged(object sender, SelectionChangedEventArgs e)
        {
       
            var dataGrid = sender as DataGrid;
            if (dataGrid != null)
            {
                _taskItem = dataGrid.SelectedItem as TaskData;           
            }
        }

        private void RefreshData(object sender, RoutedEventArgs e)
        {
            GetData();
        }

        private void DeleteTask(object sender, RoutedEventArgs e)
        {
            if (_taskItem != null)
            {
                var taskDataModel = new TaskDataModel();
                taskDataModel.Remove(_taskItem.IdTask);
            }
            else MessageBox.Show("Сначала выберите поле");
        }
    }
}
