﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamExplorer.View.Task
{
    /// <summary>
    /// Логика взаимодействия для AddTask.xaml
    /// </summary>
    public partial class AddTask : Window
    {
        public AddTask()
        {
            InitializeComponent();
        }

        private void Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AddTaskElement(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(TaskName.Text))
            {
                var task = new TaskData
                {

                    Status = Status.Text,
                    DateCreate = DateCreate.SelectedDate.Value,
                    DateEndTask = DateEndTask.SelectedDate.Value,
                    Time = Time.SelectedDate.Value
                };
                var taskDataModel = new TaskDataModel();
                taskDataModel.Get();
                if (taskDataModel.AddNew(task))
                {
                    taskDataModel.Set();
                    MessageBox.Show("Успешно добавлен");
                }
                else MessageBox.Show("Возникла ошибка");
            }
            else MessageBox.Show("Введите имя таска");
        }
    }
}
