﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGroupPr
{
    public class UserTeamLead
    {
        public TaskData AddTask(TaskData td, IdTask id)
        {
            td.taskID = id;
        }

        public TaskData DeleteTask(TaskData td, IdTask id)
        {
            if ( td.taskID == id )
                 td.taskID = null;
        }

        public TaskData UpdateTask(TaskData td, IdTask id, Data date)
        {
            if (td.taskID == id)
                td.taskData = date;
        }
    }
}
