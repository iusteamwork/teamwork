﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamExplorer
{
    interface IStorage<T>
    {
        void CreateTable(List<T> obj);
        List<T> ReadTable();
        void UpdateTable(List<T> obj);
    }

    interface ITableDataModel<T>
    {
        bool AddNew(T obj);
       bool Remove(string id);
        List<T> Find(T obj);
        void Update(T obj);
    }

}
