﻿using System.Collections.Generic;

namespace TeamExplorer
{
    public class TeamLead
    {
        private UserData _teamLead;

        public TeamLead()
        {
            _teamLead = new UserData();
        }

        public List<TaskData> FindTask(TaskData obj)
        {
            var workDataModel = new WorkDataModel();
            var tasks = new List<TaskData>();
            var taskDataModel = new TaskDataModel();
            workDataModel.Get();
            taskDataModel.Get();
            List<WorkData> workData = workDataModel.Find(new WorkData() { IdUser = _teamLead.IdUser });
            foreach (var work in workData)
            {
                tasks.AddRange(
                    taskDataModel.Find(new TaskData()
                    {
                        DateCreate = obj.DateCreate,
                        DateEndTask = obj.DateEndTask,
                        IdProject = obj.IdProject,
                        Status = obj.Status,
                        TaskName = obj.TaskName,
                        Time = obj.Time,
                        IdTask = work.IdTask
                    }));
            }
            return tasks;
        }

        public void AddTask(TaskData obj)
        {
            var workDataModel = new WorkDataModel();
            var taskDataModel = new TaskDataModel();
            taskDataModel.Get();
            workDataModel.Get();
            taskDataModel.AddNew(new TaskData()
            {
                DateCreate = obj.DateCreate,
                DateEndTask = obj.DateEndTask,
                IdProject = obj.IdProject,
                Status = obj.Status,
                TaskName = obj.TaskName,
                Time = obj.Time,
                IdTask = obj.IdTask
            });
            workDataModel.AddNew(new WorkData()
            {
                IdTask = obj.IdTask,
                IdUser = _teamLead.IdUser,
                //  IdWorks=
                //  TimeWork=
            });
        }

        public void DeleteTask(TaskData obj)
        {
            var taskDataModel = new TaskDataModel();
            taskDataModel.Get();
            taskDataModel.Remove(obj.IdTask);
        }
    }
}

