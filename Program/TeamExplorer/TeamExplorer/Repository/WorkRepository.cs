﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TeamExplorer
{
    class WorkRepository:IStorage<WorkData>
    {
        XmlSerialize<WorkData> xmls = new XmlSerialize<WorkData>("Data/WorkRepository.XML"); 
        public void CreateTable(List<WorkData> data)
        {
           xmls.Serialize(data);
        }

        public List<WorkData> ReadTable()
        {
            return xmls.Deserialize();
        }

        public void UpdateTable(List<WorkData> data)
        {
           xmls.ClearXmlFile();
           xmls.Serialize(data);
        }
    }
}
