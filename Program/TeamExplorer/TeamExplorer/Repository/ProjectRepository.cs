﻿using System.Collections.Generic;

namespace TeamExplorer.Repository
{
    public class ProjectRepository:IStorage<ProjectData>
    {
        readonly XmlSerialize<ProjectData> _xmls = new XmlSerialize<ProjectData>("Data/ProjectRepository.XML"); 
        
        public void CreateTable(List<ProjectData> data)
        {
            _xmls.Serialize(data);
        }

        public List<ProjectData> ReadTable()
        {
            return _xmls.Deserialize();
        }

        public void UpdateTable(List<ProjectData> data)
        {
            _xmls.ClearXmlFile();
            _xmls.Serialize(data);
        }
    }
}
