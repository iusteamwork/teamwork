﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace TeamExplorer.Repository
{
    class UserTypeRepository:IStorage<UserTypeData>
    {
        XmlSerialize<UserTypeData> xmls = new XmlSerialize<UserTypeData>("Data/UserTypeRepository.XML");

        public void CreateTable(List<UserTypeData> data)
        {
           xmls.Serialize(data);
        }

        public List<UserTypeData> ReadTable()
        {
            return xmls.Deserialize();
        }

        public void UpdateTable(List<UserTypeData> data)
        {
           xmls.ClearXmlFile();
           xmls.Serialize(data);
        }
    }
}
