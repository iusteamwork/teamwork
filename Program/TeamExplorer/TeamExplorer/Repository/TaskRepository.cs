﻿using System.Collections.Generic;

namespace TeamExplorer.Repository
{
    class TaskRepository:IStorage<TaskData>
    {
        XmlSerialize<TaskData> xmls = new XmlSerialize<TaskData>("Data/TaskRepository.XML");

        public void CreateTable(List<TaskData> data)
        {
           xmls.Serialize(data);
        }

        public List<TaskData> ReadTable()
        {
            return xmls.Deserialize();
        }

        public void UpdateTable(List<TaskData> data)
        {
           xmls.ClearXmlFile();
           xmls.Serialize(data);
        }
    }
}
