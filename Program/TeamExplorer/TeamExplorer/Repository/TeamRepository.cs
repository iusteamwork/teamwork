﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TeamExplorer
{
    public class TeamRepository:IStorage<TeamData>
    {
        XmlSerialize<TeamData> xmls = new XmlSerialize<TeamData>("Data/TeamRepository.XML");
        public void CreateTable(List<TeamData> data)
        {
            xmls.Serialize(data);
        }

        public List<TeamData> ReadTable()
        {
            return xmls.Deserialize();
        }

        public void UpdateTable(List<TeamData> data)
        {
           xmls.ClearXmlFile();
           xmls.Serialize(data);
        }
    }
}
