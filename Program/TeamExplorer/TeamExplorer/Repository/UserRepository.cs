﻿using System.Collections.Generic;

namespace TeamExplorer.Repository
{
    public class UserRepository:IStorage<UserData>
    {
        XmlSerialize<UserData> xmls = new XmlSerialize<UserData>("Data/UserRepository.XML");
        public void CreateTable(List<UserData> data)
        {
           xmls.Serialize(data);
        }

        public List<UserData> ReadTable()
        {
            return xmls.Deserialize();
        }
        public void UpdateTable(List<UserData> data)
        {
            xmls.ClearXmlFile();
            xmls.Serialize(data);
        }
    }
}
