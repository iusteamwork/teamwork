﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamExplorer.DataModal;

namespace TeamExplorer
{
    public class UserDeveloper
    {
        private UserData _developer;

        public UserDeveloper()
        {
            _developer = new UserData();
        }

	    public bool UserAvtorizetid(string login, string passvord)
	    {
            if (!String.IsNullOrWhiteSpace(login) && !String.IsNullOrWhiteSpace(passvord))
            {
                _developer.Login = login;
                _developer.Password = passvord;
            }
            else
            {
                return false;
            }
            UserDataModel users = new UserDataModel();
            List<UserData> user = users.Find(_developer);
            if (user != null)
            {
                _developer = user.First();
                return true;
            }
            else
            {
                return false;
            }
	    }

	    public void StartTask(string idTask)
	    {
		    throw new System.NotImplementedException();
	    }

        public void StopTask()
        {
            throw new System.NotImplementedException();
        }

	    public List<TaskData> FindTasks()
	    {
            WorkDataModel workModalUser = new WorkDataModel();
            workModalUser.Get();
            List<WorkData> worksUser = workModalUser.Find(new WorkData() { IdUser = _developer.IdUser });
            List<TaskData> tasks = new List<TaskData>();
            TaskDataModel taskModal = new TaskDataModel();
            taskModal.Get();
            foreach (var work in worksUser)
            {
                tasks.AddRange(taskModal.Find(new TaskData(){ IdTask = work.IdTask}));
            }
            return tasks;
	    }

	    public List<TaskData>  FindTasks(TaskData obj)
	    {
            WorkDataModel workModalUser = new WorkDataModel();
            workModalUser.Get();
            List<WorkData> worksUser = workModalUser.Find(new WorkData() { IdUser = _developer.IdUser });
            List<TaskData> tasks = new List<TaskData>();
            TaskDataModel taskModal = new TaskDataModel();
            taskModal.Get();
            foreach (var work in worksUser)
            {
                tasks.AddRange(taskModal.Find(new TaskData() {DateCreate = obj.DateCreate, DateEndTask = obj.DateEndTask, IdProject = obj.IdProject, Status = obj.Status, TaskName = obj.TaskName, Time = obj.Time, IdTask = work.IdTask }));
            }
            return tasks;
	    }
    }
}
